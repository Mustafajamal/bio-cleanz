<?php
/**
 * Plugin Name:       Quote Generator
 * Plugin URI:        logicsbuffer.com/
 * Description:       Custom Quotation generator for LED Panels [quote_generator]
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Mustafa jamal
 * Author URI:        logicsbuffer.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       quote-generator
 * Domain Path:       /languages
 */


add_action( 'init', 'custom_fancy_product' );

function custom_fancy_product() {

	//add_shortcode( 'show_custom_fancy_product', 'custom_fancy_product_form' );
	add_shortcode( 'quote_generator', 'custom_fancy_product_form_single' );
	add_action( 'wp_enqueue_scripts', 'custom_fancy_product_script' );
	//add_action( 'wp_ajax_nopriv_post_love_calculateQuote', 'post_love_calculateQuote' );
	//add_action( 'wp_ajax_post_love_calculateQuote', 'post_love_calculateQuote' );
	// Setup Ajax action hook
	add_action( 'wp_ajax_read_me_later', array( time(), 'read_me_later' ) );
	add_action( 'wp_ajax_nopriv_read_me_later', array( time(), 'read_me_later' ) );
}



 function custom_fancy_product_script() {
		        
	wp_enqueue_script( 'rt_custom_fancy_script', plugins_url().'/custom_fancy_product/js/custom_fancy1.js',array(),time());	
	wp_enqueue_script( 'rt_masking_script', plugins_url().'/custom_fancy_product/js/masking.js');
	wp_enqueue_style( 'rt_custom_fancy_style', plugins_url().'/custom_fancy_product/css/custom_fancy.css');

	if( is_page( 'custom-banner')){
	wp_enqueue_style( 'rt_fancy_bootstrap', plugins_url().'/custom_fancy_product/css/bootstrap1.css');
	}
}



//Test

//add_action( 'woocommerce_before_calculate_totals', 'add_custom_price' );

// add_action( 'woocommerce_before_calculate_totals', 'add_custom_price' );
// function add_custom_price( $cart_object ) {
// 	global $post;
// 	$product_id = $post->ID;    
//     $myPrice = get_post_meta('total_price_quote');

//     // Get the WC_Product Object instance
// 	$product = wc_get_product( $product_id );

// 	// Set the product active price (regular)
// 	$product->set_price( $myPrice );
// 	$product->set_regular_price( $myPrice ); // To be sure

// 	// Save product data (sync data and refresh caches)
// 	$product->save();

// }
//Test
// function woocommerce_custom_price_to_cart_item( $cart_object ) {  
//     if( !WC()->session->__isset( "reload_checkout" )) {
//         foreach ( $cart_object->cart_contents as $key => $value ) {
//             if( isset( $value["custom_price"] ) ) {
//                 //for woocommerce version lower than 3
//                 //$value['data']->price = $value["custom_price"];
//                 //for woocommerce version +3
//                 $value['data']->set_price($value["custom_price"]);
//             }
//         }  
//     }  
// }
// add_action( 'woocommerce_before_calculate_totals', 'woocommerce_custom_price_to_cart_item', 99 );

function read_me_later(){
			

			$total_price = $_REQUEST['total_price'];
			update_post_meta('total_price_quote', $total_price);
			
			// global $post;
			// global $woocommerce;

			// $product_id = $post->ID;
			// echo $product_id;
			// WC()->cart->add_to_cart($product_id);
// Simple, grouped and external products
// add_filter('woocommerce_product_get_price', array( $this, 'custom_price' ), 99, 2 );
// add_filter('woocommerce_product_get_regular_price', array( $this, 'custom_price' ), 99, 2 );

}


//Custom Price Functions

//Custom Price Functions



function custom_fancy_product_form_single() {
				$plugins_url =  plugins_url( 'images/icons/', __FILE__ );
				global $post;
				global $woocommerce;
				$product_id = $post->ID;

				if(isset($_POST['submit_prod'])){
						
					//$total_qty = $_POST['rt_qty'];			 
					//$quantity = 10;			 
					//update_post_meta($new_post_id, 'width_finishing', $rt_finishing);
					$rt_total_price = $_POST['pricetotal_quote'];
					
					echo $rt_total_price;
					echo $product_id;
					
					//Set price
					global $post;
					$product_id = $post->ID;    
				    // $myPrice = get_post_meta('total_price_quote');

				    // Get the WC_Product Object instance
					// $product = wc_get_product( $product_id );

					// Set the product active price (regular)
					// $product->set_price( $myPrice );
					// $product->set_regular_price( $myPrice ); // To be sure

					// Save product data (sync data and refresh caches)
					//$product->save();
					//Set price end

					//die();
					// Cart item data to send & save in order
					//  $cart_item_data = array('custom_price' => $rt_total_price);   
					// // woocommerce function to add product into cart check its documentation also 
					// // what we need here is only $product_id & $cart_item_data other can be default.
					//  WC()->cart->add_to_cart( $product_id, $cart_item_data);
					// // Calculate totals
					//  WC()->cart->calculate_totals();
					// //  Save cart to session
					//  WC()->cart->set_session();
					// // Maybe set cart cookies
					//  WC()->cart->maybe_set_cart_cookies();

					//add_filter( 'woocommerce_add_cart_item' , 'set_woo_prices');
					//add_filter( 'woocommerce_get_cart_item_from_session',  'set_session_prices', 20 , 3 );
					

				
				}


				// function set_woo_prices( $woo_data ) {
				//   if ( ! isset( $_POST['pricetotal_quote'] ) || empty ( $_POST['pricetotal_quote'] ) ) { return $woo_data; }
				//   $woo_data['data']->set_price( $_POST['pricetotal_quote'] );
				//   $woo_data['my_price'] = $_POST['pricetotal_quote'];
				//   return $woo_data;
				// }

				// function  set_session_prices ( $woo_data , $values , $key ) {
				//     if ( ! isset( $woo_data['my_price'] ) || empty ( $woo_data['my_price'] ) ) { return $woo_data; }
				//     $woo_data['data']->set_price( $woo_data['my_price'] );
				//     return $woo_data;
				// }



				ob_start();
				//$page_title = get_the_title();
				//$terms = get_the_terms( get_the_ID(), 'product_cat' );
				//$product_type = $terms[0]->slug;
				?>
				<script type="text/javascript">
				    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
				</script>				
<div class="custom_calculator single_product">
	<h3 class="calculator_title">Price Calculator</h3>
	<div class="orderform" id="form1">
	
		<form role="form" action="" method="post" id="add_cart" enctype="multipart/form-data" method="post">						
					    						
				<!-- <span id="price"/></span> -->
				<input type="hidden" id="eprice" />
				<input type="hidden" id="hprice" />						
				<input type="hidden" id="psqft" />						
				<input type="hidden" id="efinal" />
				<input type="hidden" id="qfinal" />
				<input type="hidden" id="sqft_total" />
				<input type="hidden" id="eyeletsT1" />
				<input type="hidden" id="hammingT1" />


			<?php  //$bio_units = get_post_meta(get_the_ID(), 'units', true); ?>
			<?php  $price_1_min = get_post_meta(get_the_ID(), 'price_1_min', true); ?>
			<?php  $price_1_max = get_post_meta(get_the_ID(), 'price_1_max', true); ?>
			<?php  $price_1 = get_post_meta(get_the_ID(), 'price_1', true); ?>

			<?php  $price_2_min = get_post_meta(get_the_ID(), 'price_2_min', true); ?>
			<?php  $price_2_max = get_post_meta(get_the_ID(), 'price_2_max', true); ?>
			<?php  $price_2 = get_post_meta(get_the_ID(), 'price_2', true); ?>

			<?php  $price_3_min = get_post_meta(get_the_ID(), 'price_3_min', true); ?>
			<?php  $price_3_max = get_post_meta(get_the_ID(), 'price_3_max', true); ?>
			<?php  $price_3 = get_post_meta(get_the_ID(), 'price_3', true); ?>

			
			<input type="hidden" value="<?php echo $price_1_min ?>" id="price_1_min" />
			<input type="hidden" value="<?php echo $price_1_max ?>" id="price_1_max" />
			<input type="hidden" value="<?php echo $price_1 ?>" id="price_1" />

			<input type="hidden" value="<?php echo $price_2_min ?>" id="price_2_min" />
			<input type="hidden" value="<?php echo $price_2_max ?>" id="price_2_max" />
			<input type="hidden" value="<?php echo $price_2 ?>" id="price_2" />

			<input type="hidden" value="<?php echo $price_3_min ?>" id="price_3_min" />
			<input type="hidden" value="<?php echo $price_3_max ?>" id="price_3_max" />
			<input type="hidden" value="<?php echo $price_3 ?>" id="price_3" />


			<!--<input type="hidden" value="<?php //echo $packets_val ?>" id="packets_val" />
			<input type="hidden" value="<?php //echo $cases_val ?>" id="cases_val" />-->

			<div class="row widthheight_main">
				<div class="col-sm-4 wh_label">
					<label class="control-label" for="width">Units</label>
					 <div class="packets_inner">
						<!-- <span class="button-icon decrease packets_dec"><i class="porto-icon-minus"></i></span> -->
						<input onKeyup="calculateQuote()" placeholder="0.00" min="1" max="9999" oninput="this.value=this.value.slice(0,this.maxLength||1/1);this.value=(this.value   < 1) ? (1/1) : this.value;" type="number" name="bio_units" class="form-control" id="bio_units" value="1" required>
						<!-- <span class="button-icon increase packets_inc"><i class="porto-icon-plus"></i></span>  -->
					</div>	
				</div>
			
				<!-- <div class="col-sm-4 wh_label">
					<label class="control-label"  for="height">Cases</label>
					 <div class="cases_inner">-->
					<!--	<span class="button-icon decrease cases_dec"><i class="porto-icon-minus"></i></span>-->
						<!--<input value="0" onKeyup="calculateQuote()"  placeholder="0.00" type="number" name="bio_cases" class="form-control" id="bio_cases" required>-->
						<!-- <span class="button-icon increase cases_inc"><i class="porto-icon-plus"></i></span>-->
					<!--</div>		
				</div>-->
			</div>
			<!--<div class="row calculations_main">
				<div class="col-sm-3"><strong>Panel Cost:</strong> <span id="panel_cost_total"></span></div>
				<div class="col-sm-3"><strong>Bracket Cost:</strong> <span id="bracket_cost_total"></span></div>
				<div class="col-sm-3"><strong>Case Cost:</strong> <span id="case_cost_total"></span></div>
				<div class="col-sm-3"><strong>Processor Cost:</strong> <span id="processor_cost_total"></span></div>
			</div>-->
			

			<div style="display:none;" class="sqft_oneway">	
				<div class="form-group">
					<input placeholder="0"  type="hidden" name="sqft" class="form-control" id="sqft" readonly>									
					<input placeholder="0"  type="hidden" name="sqfttotal" class="form-control" id="sqfttotal" readonly>									
				</div>
			</div>

			<!-- Table of Quantity -->
			<br>
			<div class="quantity_table_heading">Quantity Table</div>
			<table class="table table-bordered table-hover">
			  <thead>
			    <tr>
			      <td scope="col">Unit <?php echo $price_1_min ?> <?php //if($price_1_min != $price_1_max){echo"-".$price_1_max; } ?> </td>
			      <td scope="col">Unit <?php echo $price_2_min ?> <?php //if($price_2_min != $price_2_max){echo"-".$price_2_max; } ?> </td>
			      <td scope="col">Unit <?php echo $price_3_min ?> <?php //if($price_3_min != $price_3_max){echo"-".$price_3_max; } ?> </td>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <th colspan="1" scope="col"><?php echo $price_1 ?></th>
			      <th colspan="1" scope="col"><?php echo $price_2 ?></th>
			      <th colspan="1" scope="col"><?php echo $price_3 ?></th>
			    </tr>
			  </tbody>
			</table>
			<!-- End Table of Quantity -->
			
			<div class="row total_row_main">	
				<div class="col-sm-12">
					<div class="pricetotal_quote" >				
						<input placeholder="0.00" type="text" value="<?php echo $price_1 ?>" name="pricetotal_quote" class="form-control" id="pricetotal_quote" readonly=""> 	
						<div id="total_cart_main"></div>
					</div>				
						<input style="display:none;" placeholder="0" type="text" name="price_per_banner" value="9.4" class="form-control" id="price_per_banner" readonly="">
						<input style="display:none;" placeholder="0" type="text" name="price" class="form-control" id="price" readonly="">
				</div>	
			</div>
			<div class="row add_cart_row_main" style="display: none;">
				<div class="col-sm-12">
					<input type="submit" name="submit_prod" value="Add to Cart" class="btn-addcart" id="btn_addcart_custom">				
				</div>		
			</div>
		
	</form>
	</div>
</div>
	
<?php 
return ob_get_clean();
}


/*
add_action( 'woocommerce_before_add_to_cart_form', 'woocommerce_single_variation_add_to_cart_button', 20 );
function woocommerce_single_variation_add_to_cart_button(){
    echo do_shortcode('[quote_generator]');
}*/