<?php

add_action( 'wp_enqueue_scripts', 'porto_child_css', 1001 );

// Load CSS
function porto_child_css() {
	// porto child theme styles
	wp_deregister_style( 'styles-child' );
	wp_register_style( 'styles-child', esc_url( get_stylesheet_directory_uri() ) . '/style.css' );
	wp_enqueue_style( 'styles-child' );

	if ( is_rtl() ) {
		wp_deregister_style( 'styles-child-rtl' );
		wp_register_style( 'styles-child-rtl', esc_url( get_stylesheet_directory_uri() ) . '/style_rtl.css' );
		wp_enqueue_style( 'styles-child-rtl' );
	}
}
add_filter( 'woocommerce_cart_item_name', 'showing_sku_in_cart_items', 99, 3 );
function showing_sku_in_cart_items( $item_name, $cart_item, $cart_item_key  ) {
    // The WC_Product object
    $product = $cart_item['data'];
    // Get the  SKU
    $sku = $product->get_sku();

    // When sku doesn't exist
    if(empty($sku)) return $item_name;

    // Add the sku
    $item_name .= '<br><small class="product-sku">' . __( "Product Code: ", "woocommerce") . $sku . '</small>';

    return $item_name;
}

function cw_change_product_price_display( $price ) {
    $price .= ' <span style="font-size:0.5em;">(Excl. VAT)</span>';
    return $price;
}

add_filter( 'woocommerce_get_price_html', 'cw_change_product_price_display' );
add_filter( 'woocommerce_cart_item_price', 'cw_change_product_price_display' );

function translate_woocommerce($translation, $text, $domain) {
    if ($domain == 'woocommerce') {
        switch ($text) {
            case 'SKU':
                $translation = 'Product Code:';
                break;
            case 'SKU:':
                $translation = 'Product Code:';
                break;
        }
    }
    return $translation;
}
 
add_filter('gettext', 'translate_woocommerce', 10, 3);





// Add custom price to cart
add_action('woocommerce_after_add_to_cart_button', 'add_check_box_to_product_page', 30 );
function add_check_box_to_product_page(){ ?>     
       <div style="margin-top:20px">           
<label for="extra_price" style="display: none;"> <?php _e( 'Extra Price', 'quadlayers' ); ?>
<input type="text" name="extra_price" id="extra_price" value=""> 
</label>
                    
</div>
     <?php
}
add_filter( 'woocommerce_add_cart_item_data', 'add_cart_item_data', 10, 3 );
 
function add_cart_item_data( $cart_item_data, $product_id, $variation_id ) {
     // get product id & price
    $product = wc_get_product( $product_id );
    $price = $product->get_price();
    // extra pack checkbox
    if( ! empty( $_POST['extra_price'] ) ) {
       
        $cart_item_data['new_price'] = $_POST['extra_price'];
    }
return $cart_item_data;
}
add_action( 'woocommerce_before_calculate_totals', 'before_calculate_totals', 10, 1 );
 
function before_calculate_totals( $cart_obj ) {
if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
return;
}
// Iterate through each cart item
foreach( $cart_obj->get_cart() as $key=>$value ) {
if( isset( $value['new_price'] ) ) {
$price = $value['new_price'];
$value['data']->set_price( ( $price ) );
}
}
}



/**
 * Output engraving field.
 */
function iconic_output_engraving_field() {
    global $product;
    
    ?>
    <div class="iconic-engraving-field">

        <!--<input type="text" id="iconic-engraving" name="iconic-engraving"  >-->
        <input type="hidden" id="cart_bio_units" name="cart_bio_units" value="1">
        <!--<input type="hidden" id="cart_cases" name="cart_cases" >-->
    </div>
    <?php
}
add_action( 'woocommerce_before_add_to_cart_button', 'iconic_output_engraving_field', 10 );


function iconic_add_engraving_text_to_cart_item( $cart_item_data, $product_id, $variation_id ) {

    $cart_bio_units = filter_input( INPUT_POST, 'cart_bio_units' );

    if ( empty( $cart_bio_units ) ) {
        return $cart_item_data;
    }
    $cart_item_data['cart_bio_units'] = $cart_bio_units;

    return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'iconic_add_engraving_text_to_cart_item', 10, 3 );

function iconic_display_engraving_text_cart( $item_data, $cart_item ) {
    if ( empty( $cart_item['cart_bio_units'] ) ) {
        return $item_data;
    }
    $item_data[] = array(
        'key'     => __( 'Units', 'iconic' ),
        'value'   => wc_clean( $cart_item['cart_bio_units'] ),
        'display' => '',
    );
    return $item_data;
}
add_filter( 'woocommerce_get_item_data', 'iconic_display_engraving_text_cart', 10, 2 );

function iconic_add_engraving_text_to_order_items( $item, $cart_item_key, $values, $order ) {

    if ( empty( $values['cart_bio_units'] ) ) {
        return;
    }
    $item->add_meta_data( __( 'Units', 'iconic' ), $values['cart_bio_units'] );


}
add_action( 'woocommerce_checkout_create_order_line_item', 'iconic_add_engraving_text_to_order_items', 10, 4 );

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['seller'] );          // Remove the description tab
    return $tabs;
}
